import React from 'react';
import App from './App';
import { shallow } from 'enzyme';

describe( 'App.js TEST', () => {
	it( 'shallow', function() {
		const wrapper = shallow( <App /> );
		expect( wrapper.find( 'div' ).length ).toBeGreaterThanOrEqual( 1 );
	} );
} );