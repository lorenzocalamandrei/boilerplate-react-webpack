import React, { Component } from 'react';
import Layout from './container/Layout/Layout';

import classes from './App.css';

class App extends Component {
	render() {
		return (
			<div className={classes.App}>
				<Layout>
					Code here...
				</Layout>
			</div>
		);
	}
}

export default App;