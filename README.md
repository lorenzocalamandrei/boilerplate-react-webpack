# Boilerplate React + Webpack

## Structure
The main project folder is /src.
The environment variables must be saved in the .env file.
The tests can be structured as best you prefer, placing them all inside the "tests" folder or modular within the folder of the module itself.
They are all named "nomefile.test.js" or "nomefile.spec.js"

## Getting Started
npm i

## Run locally
* npm run start
* Visit http://localhost:8080/

## Deploy
* *npm run build* for development build or *npm run build:prod* for the production build 
* Visit by file or web server for file /builded.html

## After deploy
Use dist JS created and link all src and project for new spec.

## Available Commands
- npm i | npm install
- npm run start                     => webpack-dev-server --mode development --hot --history-api-fallback --devtool inline-source-map 
- npm run build                     => webpack --mode development --devtool inline-source-map
- npm run build:prod                => cross-env NODE_ENV=production webpack --mode production
- npm run test                      => jest
- npm run test:coverage             => jest --coverage
- npm run test:coverage:silent      => jest --coverage --silent

### Author
- Lorenzo Calamandrei