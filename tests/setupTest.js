import { configure } from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';

configure( {adapter: new Adapter()} );

const mockEnv = {
	// ENV VARIABLES FOR TESTS HERE
};

process.env = mockEnv;
