const path = require( 'path' );
const ExtractTextPlugin = require( 'extract-text-webpack-plugin' );
const Dotenv = require( 'dotenv-webpack' );
var OptimizeCssAssetsPlugin = require( 'optimize-css-assets-webpack-plugin' );

module.exports = {
	entry: {
		app: [
			'react-hot-loader/patch',
			'./src/index.js'
		],
	},
	devServer: {
		hot: true
	},
	module: {
		rules: [
			{
				test: /\.js$/,
				exclude: /node_modules/,
				use: {
					loader: "babel-loader"
				}
			},
			{
				test: /\.css$/,
				use: ['css-hot-loader'].concat( ExtractTextPlugin.extract( {
					fallback: 'style-loader',
					use: [
						{
							loader: 'css-loader',
							options: {
								minimize: true,
								modules: true,
								localIdentName: "[name]-[local]-[hash:base64:6]"
							}
						}
					]
				} ) ),
			},
			{
				test: /\.(png|jpg|svg)$/,
				use: {
					loader: "url-loader"
				}
			}
		]
	},
	output: {
		path: path.resolve( __dirname, "dist" ),
		filename: "main.js"
	},
	plugins: [
		new ExtractTextPlugin( 'main.css' ),
		new Dotenv(),
		new OptimizeCssAssetsPlugin( {
			assetNameRegExp: /main.css$/g,
			cssProcessor: require( 'cssnano' ),
			cssProcessorPluginOptions: {
				preset: ['default', {discardComments: {removeAll: true}}],
			},
			canPrint: true
		} )
	]
};